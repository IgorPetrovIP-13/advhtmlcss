const burger = document.querySelector('.navigation__burger');
const dropMenu = document.querySelector('.navigation__links');

window.addEventListener('resize', () => {
    if (window.innerWidth > 768 && burger.classList.contains('active')) {
        burger.classList.remove('active');
        dropMenu.classList.remove ('active');
    }
});

burger.addEventListener("click", ()=> {
    burger.classList.toggle('active');
    dropMenu.classList.toggle('active');
})

document.addEventListener('click', (ev) => {
    if (!ev.target.closest(".navigation__links") && !ev.target.closest(".navigation__burger")) {
        burger.classList.remove('active');
        dropMenu.classList.remove ('active');
    }
})